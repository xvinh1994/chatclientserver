﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace TCPconsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient("127.0.0.1", 15000);

            Stream s = client.GetStream();
            StreamReader myRead = new StreamReader(s);
            StreamWriter myWrite = new StreamWriter(s);
            myWrite.AutoFlush = true;

            Console.WriteLine(myRead.ReadLine());

            while (true)
            {
                Console.Write("Input: ");
                string input = Console.ReadLine();
                myWrite.WriteLine(input);
                Console.WriteLine(myRead.ReadLine() + "\n");
                if (input == "exit")
                {
                    Console.WriteLine("Exiting.....");
                    Thread.Sleep(3000);
                    break;
                }
            }
            s.Close();
            client.Close();
            Environment.Exit(0);
        }
    }
}
