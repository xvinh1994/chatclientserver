﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace TCPconsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient("116.100.138.228", 15000);

            Stream s = client.GetStream();
            StreamWriter myWrite = new StreamWriter(s);
            myWrite.AutoFlush = true;

            while (true)
            {
                Data beSent = createNewData();

                string json = JsonConvert.SerializeObject(beSent);
                Console.WriteLine(json);
                myWrite.WriteLine(json);
            }
            s.Close();
            client.Close();
            Environment.Exit(0);
        }

        private static Data createNewData()
        {
            Data result = new Data();

            Console.Write("Input: ");
            result.data = Console.ReadLine();

            return result;
        }

        private static string getXMLstring(Data data)
        {
            string xml = "";

            var xmlSerializer = new XmlSerializer(typeof(Data));
            StringWriter strWriter = new StringWriter();
            xmlSerializer.Serialize(strWriter, data);
            xml = strWriter.ToString();

            return xml;
        }

        private static string getExternalIp()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }
    }
}
