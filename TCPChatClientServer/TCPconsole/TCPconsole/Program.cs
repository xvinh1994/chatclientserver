﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Xml.Serialization;
using System.Xml;
using Newtonsoft.Json;

namespace TCPconsole
{
    class Program
    {
        static TcpListener listener;
        static int numConn = 0;

        static void Main(string[] args)
        {
            listener = new TcpListener(15000);
            listener.Start();

            for (int i = 0; i < 10; i++)
            {
                Thread t = new Thread(Service);
                t.Start();
            }
        }

        static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                int thisConn = 0;

                try
                {
                    Stream s = new NetworkStream(soc);
                    if (s != null)
                    {
                        numConn++;
                        thisConn = numConn;
                        Console.WriteLine("New Connection #" + numConn.ToString());
                    }

                    StreamReader myRead = new StreamReader(s);

                    while (true)
                    {
                        string json = myRead.ReadLine();
                        Console.WriteLine(json);
                        Data data = JsonConvert.DeserializeObject<Data>(json);

                        Console.WriteLine(data.data);
                    }
                    s.Close();
                }
                catch (Exception e)
                {
                    numConn--;
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Connection #" + thisConn + " has disconnected.");
                }

                soc.Close();
            }
        }
    }
}
