﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.IO;
using System.Threading;

namespace TCPconsole
{
    class Program
    {
        static TcpListener listener;
        static int numConn = 0;

        static void Main(string[] args)
        {
            listener = new TcpListener(15000);
            listener.Start();

            for (int i = 0; i < 10; i++)
            {
                Thread t = new Thread(Service);
                t.Start();
            }
        }

        static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                int thisConn = 0;
                bool exit = false;

                try
                {
                    Stream s = new NetworkStream(soc);
                    if (s != null)
                    {
                        numConn++;
                        thisConn = numConn;
                        Console.WriteLine("New Connection #" + numConn.ToString());
                    }

                    StreamReader myRead = new StreamReader(s);
                    StreamWriter myWrite = new StreamWriter(s);
                    myWrite.AutoFlush = true;
                    myWrite.WriteLine("Connected");

                    while (!exit)
                    {
                        setTag(myRead, myWrite, thisConn, out exit);
                    }
                    s.Close();
                }
                catch (Exception)
                {
                    numConn--;
                    Console.WriteLine("Connection #" + thisConn + "has disconnected.");
                }

                soc.Close();
            }
        }

        private static void setTag(StreamReader myRead, StreamWriter myWrite, int thisConn, out bool exit)
        {
            string str = myRead.ReadLine();

            if (str.Contains("exit"))
            {
                Console.WriteLine("Client #" + thisConn + " says \"Good Bye!!!\"");
                myWrite.WriteLine("Server says :\"Good Bye!!!\"");
                exit = true;
            }
            else
            {
                Console.WriteLine("Client #" + thisConn + " sends: " + str);
                myWrite.WriteLine("Server received: " + str);
                exit = false;
            }
        }
    }
}
