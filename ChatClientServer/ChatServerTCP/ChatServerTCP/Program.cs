﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using Newtonsoft.Json;

namespace ChatServerTCP
{
    class Program
    {
        static TcpListener listener = null;
        static int numConn = 0;
        static List<UserInfo> onlineList = new List<UserInfo>();
        static IDictionary<string, List<ChatData>> UnsentChatData = new Dictionary<string, List<ChatData>>();

        static void Main(string[] args)
        {
            listener = new TcpListener(15000);
            listener.Start();

            for (int i = 0; i < 1000; i++)
            {
                Thread t = new Thread(Service);
                t.Start();
            }
        }

        static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                int thisConn = numConn;

                try
                {
                    Stream s = new NetworkStream(soc);
                    if (s != null)
                    {
                        numConn++;
                        thisConn = numConn;
                        Console.WriteLine("New connection #" + thisConn.ToString());
                    }

                    StreamReader ServerRead = new StreamReader(s);
                    StreamWriter ServerResponse = new StreamWriter(s);

                    onlineList.Add(getInfoUser(ServerRead.ReadLine()));
                    responseIPofReceiver(ServerResponse, ServerRead.ReadLine());

                    while (true)
                    {
                        string jsonChatData = ServerRead.ReadLine();
                        ChatData data = JsonConvert.DeserializeObject<ChatData>(jsonChatData);

                        SendData(data, jsonChatData);
                    }

                    ServerResponse.Close();
                    ServerRead.Close();
                    s.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                soc.Close();
            }
        }

        private static UserInfo getInfoUser(string userinfo)
        {
            UserInfo info = JsonConvert.DeserializeObject<UserInfo>(userinfo);
            return info;
        }

        private static void responseIPofReceiver(StreamWriter ServerResponse, string requestString)
        {
            string[] split = requestString.Split(new string[] { "ask" }, StringSplitOptions.RemoveEmptyEntries);
            int idx = onlineList.FindIndex(x => x.user == split[1]);
            if (idx != -1)
            {
                ServerResponse.WriteLine(JsonConvert.SerializeObject(onlineList[idx]));
            }
        }

        private static void SendData(ChatData data, string jsonChatData)
        {
            TcpClient SendToReceiver = null;
            if (data.receiver.IPAdress != "") 
                SendToReceiver = new TcpClient(data.receiver.IPAdress, 15555);

            if (SendToReceiver != null)
            {
                Stream s = SendToReceiver.GetStream();

                if (s != null)
                {
                    StreamWriter ServerWriter = new StreamWriter(s);
                    ServerWriter.AutoFlush = true;

                    ServerWriter.WriteLine(jsonChatData);

                    ServerWriter.Close();
                    s.Close();
                    SendToReceiver.Close();
                }
                else
                {
                    addUnsentList(data);
                }
            }
            else
            {
                addUnsentList(data);
            }
        }

        private static void addUnsentList(ChatData data)
        {
            if (!UnsentChatData.ContainsKey(data.receiver.user))
            {
                List<ChatData> temp = new List<ChatData>();
                temp.Add(data);
                UnsentChatData.Add(data.receiver.user, temp);
            }
            else
            {
                UnsentChatData[data.receiver.user].Add(data);
            }
        }
    }
}
