﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServerTCP
{
    class ChatData
    {
        public UserInfo sender { get; set; }

        public UserInfo receiver { get; set; }

        public string dataToSend { get; set; }
    }
}
